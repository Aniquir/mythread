package com.company;

/**
 * Created by RENT on 2017-08-24.
 */
public class MyClass {

    private int number = 0;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public MyClass(int number) {
        this.number = number;
    }

    //zwieksz, inkrementuje
    public void increase(){
        number++;

    }
    //zmniejsza ten licznik, dekrementuje
    public void decrease(){
        number--;
    }

    @Override
    public String toString() {
        return "" + number;
    }
}
