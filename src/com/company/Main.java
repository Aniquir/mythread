package com.company;


//kilka wątków
public class Main {

    public static void main(String[] args) {

	MyClass first = new MyClass(0);

        for (int i = 0; i < 5; i++){
            Thread mySecondThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    first.increase();
                    System.out.println("Obrót numer " + "i" + ", jego wartosc: " + first.toString());
                }

            });
            mySecondThread.start();

            Thread myThireThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    first.decrease();
                    System.out.println("Obrót numer " + "i" + ", jego wartosc: " + first.toString());
                }
            });

            myThireThread.start();

        }

        System.out.println("----------");

        Thread myFirstThread = new Thread(new Runnable() {
            @Override
            public void run() {
                MyClass second = new MyClass(0);
                for (int i = 0; i < 20; i++){
                    second.increase();
                    System.out.println("Obrót numer " + i + ", jego wartosc: " + second.toString());
                }
            }
        });

        myFirstThread.start();

        System.out.println("----------");

        MyClass third = new MyClass(0);
        for (int i = 0; i < 20; i++){
            third.decrease();
            System.out.println("Obrót numer " + i + ", jego wartosc: " + third.toString());
        }
//----- po dodaniu klasy MyThread
        MyThread myThread = new MyThread();

        System.out.println(myThread.getState());

        myThread.start();

        for (int i = 0; i < 12; i++){
            System.out.println(myThread.getState());

            try {
                Thread.sleep(1100);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }

        System.out.println(myThread.getState());
    }
}

//w domu pobawic sie z synchronized, join itd, na slacku jest wrzucona dokumentacja o tym, slack z 24.08.2017
//ogolnie ogarnac temat wielowatków w sposob biegly
